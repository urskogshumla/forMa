/* Program utilizing a testing parser implementation of forMa. 
 * Author: Ivan Morén
 * CAS: ivmo0003
 * CS: id15imn
 */
 
#include "forma.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int min(int _x, int _y) { if (_x < _y) return _x; return _y; };
int fparse(const char * _fileRead, const char * _fileWrite);

#define MAX_FILENAME_SIZE 256

int main(int argc, char ** argv) {
  if (argc < 2) {
    printf("> ERROR: Please provide at least one filename or string to parse.\n");
    return 0;
  }
  
  int argumentIsFile = 0;
  for (int i = 1; i < argc; i++) {
    if (argumentIsFile) {
      char fileRead[MAX_FILENAME_SIZE], fileWrite[MAX_FILENAME_SIZE];
      strcpy(fileRead, argv[i]);
      strcpy(fileWrite, argv[i]);
      {
        char * firstDot = strstr(fileWrite, ".");
        if (firstDot != NULL && firstDot != fileWrite) *firstDot = '\0'; 
      }
      strcat(fileWrite, ".html");
      printf("\n> Input: \n%s\n", fileRead);
      printf("\n> Output: \n%s\n", fileWrite);
      
      printf("\n> Parsing info:\n");
      {
        int err = fparse(fileRead, fileWrite);
        if (err) {
          printf("Parser bailed with error code %d!\n", err);
        };
      }
      
      argumentIsFile = 0;
    }
    else if (!strncmp(argv[i], "-f", 2) || !strncmp(argv[i], "--file", 6)) {
      argumentIsFile = 1;
    }
    else if (!strncmp(argv[i], "-h", 2) || !strncmp(argv[i], "--help", 6)) {
      printf("Usage:\n");
      printf("forma [STRING] -f [FILE]\n");
    }
    else {
      printf("\n> Input: [string]\n");
      
      printf("\n> Parsing info:\n");
      int resultSize = strlen(argv[i]);
      char * result = (char *)malloc(sizeof(char) * resultSize);
      forMa_sparse(&result, &resultSize, argv[i]);
      printf("Free     %p\n", result);
      free(result);
    }
  };
  
  return 0;
};

int fparse(const char * _fileRead, const char * _fileWrite) {
  
  /* Open both input and output file and make sure they exist */
  FILE * inputFile = fopen(_fileRead, "r"), * outputFile = fopen(_fileWrite, "w+");
  if (inputFile == NULL) {
    printf("> ERROR: Could not find input file.\n");
    return 1;
  }
  if (outputFile == NULL) {
    printf("> ERROR: Output destination couldn't be created (do you have permission for this location?).\n");
    return 2;
  }
  
  /* Get infile as string */
  char * inputFileString;
  long inputFileSize;
  fseek(inputFile, 0, SEEK_END);
  inputFileSize = ftell(inputFile) + 2;
  rewind(inputFile);
  inputFileString = (char *)calloc((inputFileSize), sizeof(char));
  fread(inputFileString, sizeof(char), inputFileSize - 2, inputFile);
  inputFileString[inputFileSize - 1] = '\0';
  fclose(inputFile);
  
  /* Parse infile string */
  int parsedStringSize = inputFileSize;
  char * parsedString = (char *)malloc(sizeof(char) * parsedStringSize);
  forMa_sparse(&parsedString, &parsedStringSize, inputFileString);
  free(inputFileString);
  
  /* Write result to outputFile */
  for (int i = 0; parsedString[i] != '\0' || parsedString[i + 1] != '\0'; i++)
    fputc(parsedString[i], outputFile);
  fputc('\0', outputFile);
  fputc('\0', outputFile);
  
  free(parsedString);
  fclose(outputFile);
  return 0;
};
