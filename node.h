#ifndef FORMA_NODE
#define FORMA_NODE

typedef enum {
  forMa_TEXT,
  forMa_INLINE,
  forMa_BLOCK,
} forMa_nodeType;

typedef enum {
  forMa_previous   = 0,
  forMa_next       = 1,
  forMa_parent     = 2,
  forMa_firstChild = 3,
  forMa_lastChild  = 4,
} forMa_nodeLink;

typedef struct forMa_Node forMa_Node;
struct forMa_Node {
  forMa_nodeType type;
  char * value;
  int valueLength;
  forMa_Node * link[5];
};

forMa_Node *  forMa_newNode     (forMa_nodeType, const char *);
void          forMa_deleteNode  (forMa_Node *, int);
void          forMa_linkNode    (forMa_Node *, forMa_Node *);
void          forMa_appendNode  (forMa_Node *, forMa_Node *);
forMa_Node *  forMa_unlinkNode  (forMa_Node *, int);
forMa_Node *  forMa_rootNode    (forMa_Node *, forMa_nodeLink, int);

void forMa_inspectNodeTree(forMa_Node *);

#endif
