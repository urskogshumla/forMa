CC = gcc
CFLAGS = -MMD
LFLAGS =

FLAGS = -Wall -std=c99
CFLAGS += $(FLAGS)
LFLAGS += $(FLAGS)


EXECUTABLENAME=forma
BINARY=$(EXECUTABLENAME)

SOURCES = $(wildcard *.c */*.c)
OBJECTS = $(addprefix tmp/,$(notdir $(SOURCES:.c=.o)))
LINKS =

all: $(BINARY)

depend: $(SOURCES)
	makedepend -- $(CFLAGS) -- $(SOURCES)



$(BINARY): $(OBJECTS)
	$(CC) $(LFLAGS) -o $(BINARY) $(OBJECTS) $(LINKS)

tmp/%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

tmp/%.o: */%.c
	$(CC) $(CFLAGS) -c $< -o $@


clean:
	rm -fv $(OBJECTS) $(OBJECTS:.o=.d) $(OBJECTS:.o=.h.gch) $(BINARY)


run: $(BINARY)
	./$(BINARY)

mem: $(BINARY)
	(valgrind --show-reachable=yes --leak-check=full -v ./$(BINARY))


cleandepend:
	rm -fv $(OBJECTS:.o=.d)


-include $(OBJECTS:.o=.d)
