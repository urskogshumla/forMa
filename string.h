#ifndef FORMA_STRING_H
#define FORMA_STRING_H

#define forMa_String_growingRate (.3)

void forMa_appendToString       (char **, int *, const char *);
void forMa_setMinimumStringSize (char **, int *, int);

#endif
