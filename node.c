#include "node.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "string.h"

forMa_Node * forMa_newNode(forMa_nodeType type, const char * value) {
  forMa_Node * temp = (forMa_Node *)malloc(sizeof(forMa_Node));
  if (!temp) exit(6);
  
  temp->type = type;
  temp->valueLength = strlen(value) + 1;
  temp->value = (char *)malloc(temp->valueLength * sizeof(char));
  if (temp->value == NULL) exit(6);
  temp->value[0] = '\0';
  forMa_appendToString(&(temp->value), &(temp->valueLength), value);
  
  for (int i = 0; i < 5; i++) {
    temp->link[i] = NULL;
  };
  return temp;
};

void forMa_deleteNode(forMa_Node * node, int length) {
  if (!node) return;
  
  forMa_unlinkNode(node, length);
  
  if (node->link[forMa_firstChild])
    
  forMa_deleteNode(node->link[forMa_firstChild], -1);
  
  if (node->link[forMa_next])
    
  forMa_deleteNode(node->link[forMa_next], -1);
  
  if (*node->value) {
    free(node->value);
  }
  free(node);
  return;
};

void forMa_linkNode(forMa_Node * left_node, forMa_Node * right_node) {
  if (!left_node || !right_node) return;
  
  left_node = forMa_rootNode(left_node, forMa_next, -1);
  right_node = forMa_rootNode(right_node, forMa_previous, -1);
  left_node->link[forMa_next] = right_node;
  right_node->link[forMa_previous] = left_node;
  
  if (left_node->link[forMa_parent]) {
    forMa_Node * i = right_node;
    while (i->link[forMa_next]) {
      i = i->link[forMa_next];
      i->link[forMa_parent] = left_node->link[forMa_parent];
    };
    left_node->link[forMa_parent]->link[forMa_lastChild] = i;
  };
  
  return;
};

void forMa_appendNode(forMa_Node * parent_node, forMa_Node * child_node) {
  if (!parent_node || !child_node) return;
  
  if (parent_node->link[forMa_firstChild])
    forMa_linkNode(parent_node->link[forMa_firstChild], child_node);
  else
    parent_node->link[forMa_firstChild] = child_node;
  parent_node->link[forMa_lastChild] = child_node;
  
  forMa_Node *temp = child_node;
  do {
    temp->link[forMa_parent] = parent_node;
    temp = temp->link[forMa_next];
  } while (temp);
  
  return;
};

forMa_Node * forMa_unlinkNode(forMa_Node * node, int length) {
  if (!node) return NULL;
  
  forMa_Node * lastNode = forMa_rootNode(node, forMa_next, length), * restNode = NULL;
  
  if (node->link[forMa_previous]) {
    restNode = node->link[forMa_previous];
    restNode->link[forMa_next] = lastNode->link[forMa_next];
  }
  if (lastNode->link[forMa_next]) {
    restNode = lastNode->link[forMa_next];
    restNode->link[forMa_previous] = node->link[forMa_previous];
  }
  if (node->link[forMa_parent]) {
    if (restNode) {
      restNode->link[forMa_parent]->link[forMa_firstChild] = forMa_rootNode(restNode, forMa_previous, -1);
      restNode->link[forMa_parent]->link[forMa_lastChild] = forMa_rootNode(restNode, forMa_next, -1);
    }
    else {
      node->link[forMa_parent]->link[forMa_firstChild] = NULL;
      node->link[forMa_parent]->link[forMa_lastChild] = NULL;
    };
  };
  
  node->link[forMa_previous] = NULL;
  lastNode->link[forMa_next] = NULL;
  
  forMa_Node * temp = node;
  do {
    temp->link[forMa_parent] = NULL;
    temp = temp->link[forMa_next];
  } while (temp);
  
  return node;
};

forMa_Node * forMa_rootNode(forMa_Node * node, forMa_nodeLink nodeLink, int length) {
  if (!node || !node->link[nodeLink] || !length)
    return node;
  forMa_Node *temp = node;
  if (length > 0) {
    while (length-- && temp->link[nodeLink]) {
      temp = temp->link[nodeLink];
    }
  }
  else {
    while (temp->link[nodeLink]) {
      temp = temp->link[nodeLink];
    }
  }
  return temp;
  /*
  if (length < 0)
    return forMa_rootNode(node->link[nodeLink], nodeLink, -1);
  return forMa_rootNode(node->link[nodeLink], nodeLink, length - 1);*/
};

#ifndef FORMA_MODE_DEBUG
void forMa_inspectNodeTree(forMa_Node * node) {
  printf("\nTo use inspection function, compile forMa in debug mode using\n");
  printf("#define FORMA_MODE_DEBUG\n");
}
#endif
#ifdef FORMA_MODE_DEBUG
#include <termios.h>            //termios, TCSANOW, ECHO, ICANON
#include <unistd.h>     //STDIN_FILENO

void forMa_inspectNodeTree(forMa_Node * node) {
  forMa_Node * current = node;
  char ch, A[3][3], i;
  int sel[2] = {1,1}, currentLink;
  for (int x = 0; x < 3; x++) {
    for (int y = 0; y < 3; y++) {
      A[x][y] = ' ';
    };
  };
  A[1][1] = 'N';
  A[0][1] = 48 + forMa_previous;
  A[2][1] = 48 + forMa_next;
  A[1][0] = 48 + forMa_parent;
  A[1][2] = 48 + forMa_firstChild;
  A[2][2] = 48 + forMa_lastChild;
  
  static struct termios oldt, newt;

  /*tcgetattr gets the parameters of the current terminal
  STDIN_FILENO will tell tcgetattr that it should write the settings
  of stdin to oldt*/
  tcgetattr( STDIN_FILENO, &oldt);
  /*now the settings will be copied*/
  newt = oldt;

  /*ICANON normally takes care that one line at a time will be processed
  that means it will return if it sees a "\n" or an EOF or an EOL*/
  newt.c_lflag &= ~(ICANON | ECHO);          

  /*Those new settings will be set to STDIN
  TCSANOW tells tcsetattr to change attributes immediately. */
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);
  do {
    printf("\n\nNode:      '%s'\nSelection:  ", current->value);
    currentLink = A[sel[0]][sel[1]] - '0';
    if (currentLink  < 5 && currentLink >= 0 && current->link[currentLink]) {
      printf("[%c] ", currentLink + '0');
      printf(    "'%s'", current->link[currentLink]->value);
    }
    for (int y = 0; y < 3; y++) {
      printf("\n    ");
      for (int x = 0; x < 3; x++) {
        if (x == sel[0] && y == sel[1])
          printf("\e[43m");
        
        i = A[x][y];
        if (i == 'N' || (i - '0'  < 5 && i - '0' >= 0 && current->link[i - '0']))
          printf("%c", i);
        else
          printf(" ");
          
        if (x == sel[0] && y == sel[1])
          printf("\e[0m ");
        else
          printf(" ");
      };
    };
    printf("\n");
    ch = getchar();
    switch (ch) {
      case '':
      ch = getchar();
      if (ch == '[') {
        ch = getchar();
        switch (ch) {
          
          case 'A': 
          sel[1]--;
          if (sel[1] < 0) sel[1]+=3;
          break;
          
          case 'B': 
          sel[1]++;
          if (sel[1] > 2) sel[1]-=3;
          break;
          
          case 'C': 
          sel[0]++;
          if (sel[0] > 2) sel[0]-=3;
          break;
          
          case 'D': 
          sel[0]--;
          if (sel[0] < 0) sel[0]+=3;
          break;
        }
      }
      break;
      case '\n':
      if (currentLink >= 0 && currentLink < 5 && current->link[currentLink])
        current = current->link[currentLink];
    }
  } while(ch != 'q');
  
  
  /*restore the old settings*/
  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
} 
#endif
