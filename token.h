#ifndef FORMA_TOKEN_H
#define FORMA_TOKEN_H

void   forMa_sTokenize            (char **, int *, const char *, const int, const char **);
int    forMa_tokenizedSize        (char **);
void   forMa_appendToTokenized    (char **, int *, const char *, int);
int    forMa_matchToken           (int *, const char *, const int, const char **);

#endif
