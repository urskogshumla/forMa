#ifndef FORMA_H
#define FORMA_H

#include "node.h"

void         forMa_sparse(char **, int *, const char *);
int          forMa_fparse(const char *, const char *);
void         forMa_nodeToHTML(char **, int *, forMa_Node *, int);
forMa_Node * forMa_generatePlainNodeTree(const char *);

#endif
