# Rubrik

*Inledande beskrivning av text.*

## Inledning

Beskrivning av **bakgrund**.

Beskrivning av **syfte**.

## Huvudsaklig tes

Förklaring av **tes**.

* Nyckelord 1
* Nyckelord 2
* Nyckelord 3

Diskussion av för- och nackdelar.

Slutsats.

# Kontakt

"Namn Efternamn"(@nätverk.se/anv/författare) (Hemsida(@hemsida.org))
