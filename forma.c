#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "forma.h"
#include "token.h"
#include "string.h"


/*
 *  Parse a forMa string to HTML
 */
void forMa_sparse(char ** target, int * targetSize, const char * source) {
  if (!strlen(source)) {
    printf("Empty string\n");
    return;
  };
  /* Split the string into tokens */
  const char * tokens[] = {"#", "*", "@", "(", ")", " ", "\n", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};
  (*target)[0] = '\0';
  (*target)[1] = '\0';
  forMa_sTokenize(target, targetSize, source, sizeof(tokens) / sizeof(const char *), tokens);
  
  /* Parse the tokens into a node tree */
  forMa_Node * nodeTree = forMa_generatePlainNodeTree(*target);
  if (nodeTree != NULL) printf("Tree pointer is %p \n", nodeTree);
  else printf("NULL node tree!!\n");
  
  /* Parse the node tree */
  // TODO
  nodeTree->type = forMa_BLOCK;
  forMa_Node *temp = forMa_rootNode(nodeTree, forMa_next, 2);
  forMa_unlinkNode(temp, 3);
  forMa_appendNode(nodeTree, temp);
  nodeTree = forMa_rootNode(nodeTree, forMa_previous, -1);
  
  forMa_inspectNodeTree(nodeTree);
  
  /* Parse the node tree into a string */
  // TODO
  printf("\n> Parsing node tree to HTML string\n");
  (*target)[0] = '\0';
  forMa_nodeToHTML(target, targetSize, nodeTree, -1);
  printf("\n> Result:\n\n%s\n\n", *target);
  forMa_deleteNode(nodeTree, -1);
  printf("Returning string\n");
  /* Return allocated string */
  return;
};



forMa_Node * forMa_generatePlainNodeTree(const char * tokenizedString) {
  
  printf("> Generating plain node tree from tokenized string:\n");
  
  forMa_Node * startNode = forMa_newNode(forMa_TEXT, "*"), * currentNode = startNode;
  while (tokenizedString[0] != '\0' || tokenizedString[1] !='\0') {
    
    if (tokenizedString[0] == '\0') {
      tokenizedString++;
      continue;
    };
    
    forMa_linkNode(currentNode, forMa_newNode(forMa_TEXT, tokenizedString));
    if (!(currentNode->link[forMa_next])) { printf("This should Not Happen 2\n"); abort(); break;};
    currentNode = currentNode->link[forMa_next];
    
    if (tokenizedString[0] != '\n')
      printf("  ->  '%s'", tokenizedString);
    else 
      printf("  ->  '\\n'");
      
    tokenizedString += strlen(tokenizedString);
    
  };
  printf("\n");
  startNode = startNode->link[forMa_next];
  forMa_deleteNode(startNode->link[forMa_previous], 0);
  return startNode;
};



void forMa_nodeToHTML(char ** target, int * targetSize, forMa_Node * node, int length) {
  if (!node) return;
  /*
  printf("Translating node with ");
  if (node->link[forMa_previous]) printf("left, ");
  if (node->link[forMa_next]) printf("right, ");
  if (node->link[forMa_parent]) printf("parent, ");
  if (node->link[forMa_firstChild]) printf("child, ");
  printf(" and value '%s'\n", node->value);*/
  if (node->type == forMa_TEXT) {
    forMa_appendToString(target, targetSize, node->value);
  }
  else {
    forMa_appendToString(target, targetSize, "<");
    forMa_appendToString(target, targetSize, node->value);
    forMa_appendToString(target, targetSize, ">");
    if (node->link[forMa_firstChild]) {
      int childStringSize = 1;
      char * childString = (char *)malloc(sizeof(char) * childStringSize);
      childString[0] = '\0';
      forMa_nodeToHTML(&childString, &childStringSize, node->link[forMa_firstChild], -1);
      forMa_appendToString(target, targetSize, childString);
      free(childString);
    }
    forMa_appendToString(target, targetSize, "</");
    forMa_appendToString(target, targetSize, node->value);
    forMa_appendToString(target, targetSize, ">");
  }
  
  if (node->link[forMa_next]) {
    int nextStringSize = 10;
    char * nextString = (char *)malloc(sizeof(char) * nextStringSize);
    if (nextString == NULL) exit(13);
    nextString[0] = '\0';
    if (length < 0)
      forMa_nodeToHTML(&nextString, &nextStringSize, node->link[forMa_next], -1);
    else if (length)
      forMa_nodeToHTML(&nextString, &nextStringSize, node->link[forMa_next], length - 1);
    forMa_appendToString(target, targetSize, nextString);
    free(nextString);
  }
  
  return;
};
