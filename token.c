#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "token.h"
#include "string.h"

/* 
 *  Allocate and create a string of tokens from a string and a list of tokens.
 *  The tokens are separated by \0 and end with \0\0.
 */
void forMa_sTokenize(char ** target, int * targetSize, const char * source, const int tokenCount, const char * tokens[]) {
  printf("Tokenizing\n");
  if (!strlen(source)) return;
  
  //int resultSize = strlen(source) * (1 + forMa_String_growingRate);
  
  //char * result = (char *)malloc(sizeof(char) * resultSize);
  /*if (result == NULL) {
    printf("No room in memory..\n");
    return '\0';
  }*/
  forMa_setMinimumStringSize(target, targetSize, 2);
  (*target)[0] = '\0';
  (*target)[1] = '\0';
  int currentToken = 0;
  
  for (int i = 0; source[i] != '\0';) {
    
    /* Check if string matches a token */
    if (forMa_matchToken(&currentToken, &(source[i]), tokenCount, tokens)) {
      forMa_appendToTokenized(target, targetSize, tokens[currentToken], 1);
      i += (int)strlen(tokens[currentToken]);
      continue;
    };
    
    {
      char temp[2];
      temp[0] = source[i];
      temp[1] = '\0';
      forMa_appendToTokenized(target, targetSize, temp, currentToken);
    }
    currentToken = -1;
    i++;
  };
  printf("\n> Tokenizing Result (%d): \n \"\e[30m", forMa_tokenizedSize(target));
  int col = 0;
  for (int i = 0; (*target)[i] != '\0' || (*target)[i + 1] != '\0';i++)
    if ((*target)[i] == '\n') printf("\e[%dm⤶\n  ", 33 + 67 * col%2);
    else if ((*target)[i] == ' ') printf("\e[0m \e[%dm", 33 + 67 * col%2);
    else if ((*target)[i] == '\0' && (*target)[i + 1] != ' ') printf("\e[0m\e[%dm", 33 + 67 * ++col%2);
    else if ((*target)[i] == '\0') printf("\e[0m\e[%dm", 33 + 67 * col%2);
    else printf("%c", (*target)[i]);
  printf("\e[0m\"\n\n");
};


/* 
 *  Get the size of a string of tokens, exluding the two end terminators.
 */
int forMa_tokenizedSize(char ** str) {
  int i = 0;
  while (((*str)[i] != '\0' || (*str)[i + 1] != '\0')) i++;
  return i;
};


/* 
 *  Append source to the end of the tokenized string target. Preceeds
 *  appended string with \0 if newToken is positive.
 */
void forMa_appendToTokenized(char ** target, int * targetSize, const char * source, int newToken) {
  
  if (newToken >= 0) newToken = 1; else newToken = 0;
  int sourceLength = strlen(source);
    if (sourceLength == 0) return;
  int targetLength = forMa_tokenizedSize(target) + newToken;
  forMa_setMinimumStringSize(target, targetSize, targetLength + sourceLength + 2);
  
  strncat(*target + targetLength, source, sourceLength);
  
  (*target)[targetLength + sourceLength]     = '\0';
  (*target)[targetLength + sourceLength + 1] = '\0';
  return;
};


/* 
 *  Check if the beginning of str matches any token in the
 *  list tokens. At a match, 1 is returned and the token
 *  index is written to result parameter.
 */
int forMa_matchToken(int * result, const char * str, const int tokenCount, const char * tokens[]) {
  for (int i = 0; i < tokenCount; i++) {
    int j = 0;
    while (tokens[i][j] != '\0' && str[j] != '\0') {
      if (tokens[i][j] != str[j]) break;
      j++;
    };
    if (tokens[i][j] == '\0') {
      *result = i;
      return 1;
    };
  };
  return 0;
};
