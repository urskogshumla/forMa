#include "string.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* 
 *  Append source to the end of string target.
 */
void forMa_appendToString(char ** target, int * targetSize, const char * source) {
  //printf("Appending '%s' + '%s' ", target, source);
  if (source[0] == '\0') return;
  
  int targetLength = strlen(*target);
  forMa_setMinimumStringSize(target, targetSize, targetLength + strlen(source) + 1);
  strncat(*target, source, strlen(source));
  (*target)[targetLength + strlen(source)] = '\0';
  //printf("resulted in '%s'\n", target);
  return;
};


/* 
 *  Change size of string to targetSize and increase its size by forMa_string_growingRate percent.
 */
void forMa_setMinimumStringSize(char ** target, int * originalSize, int targetSize) {
  
  int spaceLeft = *originalSize - targetSize;
  if (spaceLeft < 0) {
    targetSize = (int)(targetSize * (1 + forMa_String_growingRate));
    *target = (char *)realloc(*target, (targetSize) * sizeof(char)); //FIXME Pointer doesn't change outside function bug, take away _target or return void.
    if (target == NULL) exit(5); // IFHANDLED: remember originalSize is already changed
    *originalSize = targetSize;
  };
  return;
};
